# Projet "Agence immobilière"

## Principe

Exemple de projet PHP *vanilla* avec connexion à une base de données MySQL ou PostreSQL via PDO.

## Configuration de la base de données

Pour que la base de données fonctionne, vous devez créer/configurer ce fichier : `data/config.php` :

```php
// Exemple pour MySQL sur WAMP
$dbconfig = [
    'host' => 'localhost',
    'port' => 3306,
    'database' => 'YOURDATABASE',
    'username' => 'root',
    'password' => ''
];
```

Pour aller plus vite, copiez le fichier `data/config.default.php`sous le nom `data/config.php` et modifiez les réglages du fichier `data/config.php` obtenu.
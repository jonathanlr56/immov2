<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>

    <!-- 
        Le titre ne fait pas partie de head.php parce qu'il
        doit être différent pour chaque page
    -->
    <title>Liste des biens</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!-- Contenu de la page -->
    <div class="container">
        <h1>Liste des biens</h1>

        <?php
        // Créer une instruction SQL
        $sql = "SELECT * FROM liste_biens";

        // Créer et exécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        // Récupérer les lignes de tables qui correspondent à la requête
        $listeBiens = $requete->fetchAll();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Adresse</th>
                    <th scope="col">code postal</th>
                    <th scope="col">Villes</th>
                    <th scope="col">Transaction</th>
                    <th scope="col">Type de biens</th>
                    <th scope="col"  >Pièces</th>
                    <th scope="col">Montant</th>


                </tr>
            </thead>
            <tbody>
                <?php
                // On peut maintenant afficher les données

                foreach ($listeBiens as $biens) {
                    echo  '<tr>';
                    echo '<td>' . $biens['adresse1'] . '</td>';
                    echo '<td>' . $biens['codepostal'] . '</td>';
                    echo '<td>' . $biens['nomville'] . '</td>';
                    echo '<td>' . $biens['intituletransaction'] . '</td>';
                    echo '<td>' . $biens['intitulebien'] . '</td>';
                    echo '<td class="badge badge-success p-1">' . $biens['pieces'] . '</td>';
                    echo '<td>' . $biens['montant'] . '</td>';

                    echo ' </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>
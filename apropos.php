<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>

    <!-- 
        Le titre ne fait pas partie de head.php parce qu'il
        doit être différent pour chaque page
    -->
    <title>Qui sommes-nous ?</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!-- Contenu de la page -->
    <div class="container">
        <div class="jumbotron">
            <h1>Page en construction</h1>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aut aliquid earum quibusdam veniam commodi non quisquam necessitatibus odit sunt eius officiis ducimus dolore repellendus, iusto explicabo, ipsam corrupti asperiores reprehenderit.</p>
        </div>
    </div>

    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>

    <!-- 
        Le titre ne fait pas partie de head.php parce qu'il
        doit être différent pour chaque page
    -->
    <title>Liste des propriétaires</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!-- Contenu de la page -->
    <div class="container">
        <h1>Liste des propriétaires</h1>

        <?php
        // Créer une instruction SQL
        $sql = "SELECT * FROM proprietaires";

        // Créer et exécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        // Récupérer les lignes de tables qui correspondent à la requête
        $listePersonnes = $requete->fetchAll();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Télèphone Perso</th>

                </tr>
            </thead>
            <tbody>
                <?php
                // On peut maintenant afficher les données

                foreach ($listePersonnes as $personne) {
                    echo  '<tr>';
                    echo '<td>' . $personne['nomproprietaire'] . '</td>';
                    echo '<td>' . $personne['prenomproprietaire'] . '</td>';
                    echo '<td>' . $personne['titre'] . '</td>';
                    echo '<td>' . $personne['telephonemobile'] . '</td>';
                    echo '<td>' . $personne['telephonepersonnel'] . '</td>';
                    echo ' </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>
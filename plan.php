<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>

    <!-- 
        Le titre ne fait pas partie de head.php parce qu'il
        doit être différent pour chaque page
    -->
    <title>Plan d'accès</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!-- Contenu de la page -->
    <div class="container">
        <h1>Plan d'accès</h1>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2682.3071915030478!2d-3.3727216841622627!3d47.7560955856317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48105eff3a5a965d%3A0xf7c8d67ac1f7deb8!2sChambre+de+Commerce+et+d&#39;Industrie+du+Morbihan%2C+service+formation!5e0!3m2!1sfr!2sfr!4v1458035227584" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>
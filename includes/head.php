<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- jQuery -->
<script src="lib/jquery/jquery-3.3.1.min.js"></script>

<!-- Bootstrap -->
<link type="text/css" rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css" />
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- FontAwesome -->
<link rel="stylesheet" href="lib/fontawesome/css/fontawesome-all.min.css">

<!-- Styles spécifiques -->
<link type="text/css" rel="stylesheet" href="css/styles.css" />

<?php
    // Inclure le fichier de connexion (= ouvrir la connexion)
    include "data/connection.php";
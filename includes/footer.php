<footer class="text-center">
    <strong>&copy; <?php echo date('Y'); ?> &mdash; Agence Immobilière Morbihannaise</strong>
</footer>

<!-- Librairies Javascript -->
<script src="lib/jquery/jquery-3.3.1.min.js"></script>
<script src="lib/bootstrap/bootstrap.bundle.min.js"></script>
<script src="js/scripts.js"></script>
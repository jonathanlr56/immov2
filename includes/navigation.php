<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
    <a class="navbar-brand" href="index.php">Immo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <!-- Liens du menu gauche -->
            <li class="nav-item <?php echo getActiveState('proprietaires'); ?>">
                <a class="nav-link" href="proprietaire.php">Propriétaires</a>
            </li>
            <li class="nav-item <?php echo getActiveState('biens'); ?>">
                <a class="nav-link" href="biens.php">Biens</a>
            </li>
            <li class="nav-item <?php echo getActiveState('clients'); ?>">
                <a class="nav-link" href="clients.php">Clients</a>
            </li>
        </ul>

        <!-- Liens du menu droit -->
        <ul class="navbar-nav">
            <li class="nav-item <?php echo getActiveState('apropos'); ?>">
                <a class="nav-link" href="apropos.php">Qui sommes-nous ?</a>
            </li>
            <li class="nav-item <?php echo getActiveState('contact'); ?>">
                <a class="nav-link" href="contact.php">Contact</a>
            </li>
            <li class="nav-item <?php echo getActiveState('plan'); ?>">
                <a class="nav-link" href="plan.php">Plan d'accès</a>
            </li>
        </ul>

    </div>
</nav>

<?php
// Est-ce qu'un lien est actif ?
function isActive($slug)
{
    $page = basename($_SERVER['SCRIPT_FILENAME']);
    return ($slug . '.php') === $page;
}

// Classe pour un lien actif
function getActiveState($slug)
{
    return isActive($slug) ? 'active' : '';
}

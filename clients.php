<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>

    <!-- 
        Le titre ne fait pas partie de head.php parce qu'il
        doit être différent pour chaque page
    -->
    <title>Liste des propriétaires</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!-- Contenu de la page -->
    <div class="container">
        <h1>Liste des clients</h1>

        <?php
        // Créer une instruction SQL
        $sql = "SELECT * FROM liste_client";


        // Créer et exécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        // Récupérer les lignes de tables qui correspondent à la requête
        $listeClients = $requete->fetchAll();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Code postal</th>
                    <th scope="col">Ville</th>

                </tr>
            </thead>
            <tbody>
                <?php
                // On peut maintenant afficher les données

                foreach ($listeClients as $clients) {
                    echo  '<tr>';
                    echo '<td>' . $clients['nomclient'] . '</td>';
                    echo '<td>' . $clients['prenomclient'] . '</td>';
                    echo '<td>' . $clients['titre'] . '</td>';
                    echo '<td>' . $clients['codepostal'] . '</td>';
                    echo '<td>' . $clients['nomville'] . '</td>';
                    echo ' </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>